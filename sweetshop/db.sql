USE [master]
GO

/****** Object:  Database [SweetShop]    Script Date: 30/04/2019 15:03:59 ******/
CREATE DATABASE [SweetShop]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SweetShop', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS01\MSSQL\DATA\SweetShop.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SweetShop_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS01\MSSQL\DATA\SweetShop_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SweetShop].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [SweetShop] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [SweetShop] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [SweetShop] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [SweetShop] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [SweetShop] SET ARITHABORT OFF 
GO

ALTER DATABASE [SweetShop] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [SweetShop] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [SweetShop] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [SweetShop] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [SweetShop] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [SweetShop] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [SweetShop] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [SweetShop] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [SweetShop] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [SweetShop] SET  DISABLE_BROKER 
GO

ALTER DATABASE [SweetShop] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [SweetShop] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [SweetShop] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [SweetShop] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [SweetShop] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [SweetShop] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [SweetShop] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [SweetShop] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [SweetShop] SET  MULTI_USER 
GO

ALTER DATABASE [SweetShop] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [SweetShop] SET DB_CHAINING OFF 
GO

ALTER DATABASE [SweetShop] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [SweetShop] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO

ALTER DATABASE [SweetShop] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [SweetShop] SET QUERY_STORE = OFF
GO

ALTER DATABASE [SweetShop] SET  READ_WRITE 
GO

