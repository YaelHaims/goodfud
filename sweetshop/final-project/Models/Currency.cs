﻿using System.ComponentModel;

namespace final_project.Models
{
    public enum Currency
    {
        USD = '$',
        EUR = '€',
        ILS = '₪',
    }
}