﻿using final_project.Models;
using Microsoft.EntityFrameworkCore;

namespace final_project.Data
{
    public class RetrocordContext : DbContext
    {
        public RetrocordContext(DbContextOptions<RetrocordContext> options) : base(options)
        {
        }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Costumer> Costumers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrderItem>()
                .HasOne(p => p.Order)
                .WithMany(b => b.OrderItems)
                .HasForeignKey(p => p.OrderID);

        }

        public void InitializeProducts()
        {
            var tlvBranch = new Branch
            {
                branchName = "Retrocord - TLV",
                addressInfo = "Tel Aviv, Menachem Begin 148 Blvd",
                locationX = (float)32.074031,
                locationY = (float)34.792868
            };

            var jerusalemBranch = new Branch
            {
                branchName = "Retrocord - HQ",
                addressInfo = "Jerusalem, Hommos 1 St",
                locationX = (float)31.777820,
                locationY = (float)35.209204
            };

            Branches.AddRange(tlvBranch, jerusalemBranch);

            var admin1 = new Admin
            {
                Email = "bla@gmail.com",
                FullName = "bla",
                Password = "Aa123456"
            };

            var admin2 = new Admin
            {
                Email = "bla2@gmail.com",
                FullName = "bla2",
                Password = "Aa123456"
            };

            Admins.AddRange(admin1, admin2);

            var rock = new Category
            {
                Name = "Rock"
            };

            var pop = new Category
            {
                Name = "Pop"
            };

            var metal = new Category
            {
                Name = "Metal"
            };

            var mizrahit = new Category
            {
                Name = "Middle Eastern"
            };

            var house = new Category
            {
                Name = "House"
            };

            var rap = new Category
            {
                Name = "Rap"
            };

            Categories.AddRange(rock, pop, mizrahit, metal, house, rap);

            var rihanna = new Product
            {
                Name = "Love The Way You Lie",
                Category = pop,
                IsDeleted = false,
                Price = 100,
                ImgPath = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSmsKRHhNq-hcddyxNgNCeXspuXOIO8480S9Q&usqp=CAU"
            };

            var linkinPark = new Product
            {
                Name = "In The End",
                Category = rock,
                IsDeleted = false,
                Price = 120,
                ImgPath = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxASEhUQEBIVFRUXFRUVFRUXFRUWFRUVFRUXFhUVFRUYHSggGholGxUVITEhJSkrLy4uGB8zODMtNyguLisBCgoKDg0OFxAQFy0dHR0tLS0tLSstLS0rLS0tLS0tLS0tLS0tLS0tKy0tLS0tLS0tLS0tLS0tLSstLSs3LSs3Lf/AABEIASwAqAMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAAAQIHAwUGCAT/xABWEAABAwIDBQMFCggIDQUBAAABAAIDBBEFEiEGBzFBURMiYQgUI3GzMjVCUnJzdIGCkSQzNJOxstHSFSVVYmOSocEXNkNEU1Rkg7TD0/DxlKKjwuIW/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAeEQEBAAMAAwEBAQAAAAAAAAAAAQIRMRIhQVFCIv/aAAwDAQACEQMRAD8ApFPKbXsbaAm2gJvYX8bH7imHCxFuY16DXT9H3JB5sRc24kciRext9Z+8ruyHOJ4+A6cFBSaRzF9Pu8UisqbW3vqNBfXnqBYdTqkEBNAIAQpKxCKiU1FSqnEG375IHMgBx+4kX+9QRZCgEJJqBJgISQCEIUEpGWJFwfEcD6lFCkLc/q9aoSEIQSQm63If3pLVQkFNCBJoCEDCLpITYAFFSJFvHXW+ltLaW9f9iiipB5sW3NjYkX0JF7EjwufvKihCgSE7JKATskhQSaR0ukUW5pIBCEKhkoQEIJIQktIaEIQSewi17ai41B0PW3D1JXt/4UUkU0ISQCEIQCF2Gxe72rxOCeenLB2Ra1ofoJXkXcxrvgkNynXTvDULmsVwyemkdDUxPikbxa4WPrHUdCNCpsfK1xBuNCNQeh6pEqTrX014cRb16KJKgSEJoEhCEAE0BMBIBCChaDc6/IDS2n6fWkEwE1QWUjM7KGX7ocXAcg5wAJ+5rfuUUkREpospgKKiorc7N7L1lfJ2VJC59rZn8I2A83vOg9XE8gVeeye6nD8OZ53iMkcz2AOc6SzaaK3MNd7o+LvCwCzchVew+7GuxG0luwpz/lpGnvDrEzQv9ejfHknvMwWkpKmLDKBhe+Nre1kPellnltlZoLABuSzW83nnqu72333Nbmgwptzw85e3ujxijPH1u6cCuf3K4JJiGJPxCpJkEB7Vzna56h9+zvcfBs52nAtap76q3sFpIcFwkdpa0ERklI+HKdXAHmS8ho+pc/hO02DbQxCmq42tn5RPOWQOtcmnlFi7hwFjYatstB5RG09hFhkZ42mnt0FxEw/WC63g1UaxxBuDYgggjQgjmDyUkFm7c7nKukzTUeapgGpAHp2D+cwe7Hi3XwCrEhWzsLvkqabLBiIdPELDtb+njFvhX/GDhxsfE8F3+O7F4RjsXndLI1sjv84iAuXW4TxG1zwuDZ3irseZ0zawtx5/3WXU7a7D12HECojLo9Q2dneidc3Ava7DrwdrxsuVVQJ3FuBvfjfS3qtx4c1FCBhSQELQihCFNiaaSFpAUk1uNkcFFbWQUheWCV+UvAzFoALjYX46INdR0kkj2xxMc97jZrGNLnOPQAalXDsRuTc7LNijsreIpmO7x+dkHufkt18RwXauGC7OQXtaRzdOElVPbjrpYX+S0FVDtvvWrq/NFETTU5uOzYe+8f0knE/JFhrrdY3bxVobS7yMLwmPzShjZJIy4EUNmxRuvr2kg0zXvcC5vxtxVGbV7YV2Ivz1UpLQbsibdsTPks66nvG58VoUJMQL1XsJg8eEYUDP3S1jqmpPPMW5nDxLWgN8cviqR3L7M+e4gx7xeKntNJ0LgfRM+twv4hhCsLyhdp+zgjw6M96a0kvhEx3dB+U8f+w9VMhSG0eMSVlVNVy+6leXW45RwYwHo1oa36lrgE2utfhqLagHmDpfgdOI/vVzbiNi454amrqWZmSsfSxg82uHpnjx4NBHRycFMLabPbQVVDKJqSV0buYHuXj4r2HRw9aNpsFkoqqakk91E8tv8ZvFj/tNLT9a1avR6N2L3s0Ve3zXEGMhleMhDwDTTX0IBdfLf4r9PErXbcbk4pc0+FuET9SYHn0Tufo3cWHwNxw9yFQa77YPenW4flhk/CKYWHZuPejH9E/l8k3GmluKzrSuNxbCqilldBUxOikbxa4WNuRHIg24jQr5AF6spqrB8fp7ENmAGrHDLPATz07zfWDY25qhN5OyTMMrPNmTOkjMQmYXAZ25nPYGHUAm8Yu4AaG9tFZUcekU1IynLk0tcO4C9wCBrx4E6cFoQZa4vw52Njb1pqKFBNFkMUl0QArrN1PvvR/On2b1ya6vdQf43o/nT7N6mXCOy8o4jzukB4di+/1ydFUBVueUd+WU30c+0cqiKzjxaSSF1+63Zj+EMQiie28UfppuhYwjuH5TsrfUT0Si8t0uAtw7CxLPZj5WmpncdMjMt2NdfhljAJHIly887ZY8+vrJqt17Pd3Gn4Ebe7G3+qBfxuVeW/zabzejbRRm0lSe9bi2BhBd/WOVviMy86kLOM2VnwugkqJo6eIXkke1jB/OcbC/hzJ6L1PPiNHglNRUrjZjnx0zToNTrJM/wzHM49XqsfJ62Z7SaTEZG92K8UV+crh33D5LCB9vwXN76dpvPcQexjrxU94WdC8H0z/rcMviGApfdHd+UNsvnijxONvejtFP4xuPo3n1OOX7Y6KhV6f3aYzHi+EmCp77msNNUDm4ZbNffjcsIN/jA9F5z2mwaSiqpqSX3UTy2/xm8WPHg5pafrSVWtdbkm1qTW8+X/f7FkW5EW95OH5VVfMM9otf5RPvnF9Dj9tOvv8AJv8Ayqr+ZZ7RfB5RXvnF9Dj9tOsf0KuKipiQ2LdLEg8BfS/B1rjjy46dAhjQQ4l1iBoLHvG4FvDQk69FqiCEITYypFF0rrdqaMk8Lrq90/vvR/On2b1yuQ2DtLEkcRfQAm7eIHeGvA69Cur3UD+N6P50+zes3ius8o4/h1OP9m/5r/2KpCrY8o0/h9P9FHtZFU5WceB2XpPcZs4KTD/OpABJU2lJPwYWg9kL9CC5/wBsdFRmwOzhxCuhpbHITmlI5RM1fryv7keLgr3317Qijw/zWKzZKi8LQNMsLQO1IHTKQz7fgl9+hR+8DaI4hXTVN/R37OEdImXDPVfVxHVxWggp3yPbHG0ue9wYxo4uc42aB6yQkQrP3C7NdvWOrZG+jph3b8DM8EN9eVt3eBLVu/5ifVi4zOzAMDEcZHatYImH49TLcueL9CXvt0bZeZHFWhv62k84rRSMdeOmFnW4GZ9i/wBeUZW+BzdVVxWJ+q7rc1tR5jiDWvdaGotDJ0DifRPPqcbX5Bzl33lDbLZ448TjGsdoZ/m3H0bzrycS37Y6Khl6k3d4zHjGFdlUd9wY6mqRfUnLYPv1c0h1/jX6LN/VeXkiVstpMGkoqmWkl91E8tva2ZvFjwOjmkH61q102i4vJu/Kqr5lntFr/KK984vocftp19/k2/lNX8yz2i+LyhpC3FIiOPmbBwB4yzg8fArH1VVlCaS1UCEIUEykmpWW0ZpabKxkmdhz5+4Dd7MpAu8W0vfTU3sV026j33o/nT7N65NdXuo996P50+zeplPRHTeUY7+MYR/sjP7Zpv2KqgrS8oo/xnF9Dj9tOuD2VwN9dVw0kfGR4BPxWDWR/wBTQ4rM4q8PJ+2a7GlfXyNs+oOWO/EQMPHhpmfc+Ia0qrd6G0/n+ISSsdeKP0MOtwWMJu8cu87M6/TL0XofaqgqG4c+kwyMCTshBEMwYI2WyFwJ5hl7eNlQh3P42P8AN2H1Txf3uTGze6Vw7WkmwBJOgAFySeAA6r09E6PA8GMhjjZIyJrntZe0lU9rWC51Ju/KCeg6BV9u33WV0VfHPiEAZFDeRvpIn55W/i22Y4kWJzXPxR1XW749m8TxEQ09GxhhZeR5dI1uaTVrRY62a3N/X8Fc8t+iPOdTUOkc6R7i5znFznHi5zjdxPrJKw2Vgnc3jX+hj/PR/tUTudxv/V2fn4v3k3BwAVgbl9p/MsQbG82hqbQv6B9/Qv8AqcS3wDyeSgdz+N/6sz8/D+8oO3RY5/qgP+/p/wB9S2aHe+UPsvmjjxOMax2hn+Q4+iefU4lv229FQ69e4JST1OHNp8UiLZXxGGobmY7NoWdoHMJF3CztOBPgvKm0mDSUVVNSS+6ieW34Zm8WPA6OaWu+tSVVn+Tb+U1fzLPaL4fKKH8ZxfQ4/bTr7vJt/Kav5lntF8PlE++cX0OP206Toq4qJUnFRWkCEIUGVPMkSgLqyF1m6n33o/nT7N65Wy22yeMiirIKssLxE/MWg2JBBabHrqplPSx2vlE++cf0SP2sy53dztlHhUsk5pe3kcwMYe17MRtvd/wHXJIb0tbxV4xz4HtFDYhr5A33JtHVQ342PG17cMzT4qq9tNz1bSZpaS9VCNe6LTsGvuox7u2mrfuC5SziugO/9/LD2/8AqT/0ljO/6blQR/XO4/8A0VMuH7LdF1O7XZr+EK+KAi8TfSz/ADTCLj7RLW/aV8YPSWB7QPdh4xCtY2C8Tp3MBLskVi5tyeLi2xt42VLP37YoXHJDShtzlBZKSBfQEiQXNudguq8oLaXsoI8NiIDpbSSgW0iYe40jo54v/uz1VEMamOOy1Z4354r/AKCj/Nz/APWUhv4xIcaelPqEw/5iq56xFauMibWy3f1X86Wm/wDl/eWQb+6znSQf1pP2qo7JOKz4xVvjf7V86OH+u9cNt9tf/CkzKh1OyGRrMji1znZ2g3bcHgRd2vO46BcuV22w+7OvxG0gb2EB/wAvI094f0TNDJ69B4pqQdZ5N4/Car5hntFr/KK984vocftp1bmz+zWGYHA6XOGaAS1Ezhmf0b0GvBrRr4lUNvd2pp8RrhPS5uzZC2EOcMuctfI4uA4hvfHGx04KTquKskhCqBCEKCd7rI0IYEy5dpNdZDisbkyVFLV0yU8743Nkjc5j2m7XtJa5p6tcNQVbWxe+2aLLDiTTMzgJ2ACVvy26NePEWPrKqFAXOzavTWLbJYLjsRqqd7O0P+cQ2Dw6wsJ4za5tbR4DrcCFl3b7FNwaCokqJGOe5xc+UXDWwRAltweHwnH12ubXXm/BsXqaSQTUsz4nj4TTxHRw4OHgQQu62g3vVNZhz6KSJrJXlrZJmEhr4hcuGQ6tcbNB1sQXcFmyjk9r8ddX1k1W86Pd6Npv3YwAGMtwBDQL8ibkXWourgG5B7qKGWObLVmPPLG8Axuc67msaRqwgENJ1BtfRVZi2EVFLIYaqJ0Ug1yuHEdWng4eIJC6YWfEr5IgzMO0Lg24zFoBdbnYEgE+sqLWDKSSARaw1u6/HW1ha3PqkslNTPke2OJjnvcbNYxpc5x6Bo1K1RgW12c2ZrK+TsqSJzzpmdwjYDze86D1cTyBVo7E7k3vyzYo7I3iKZh756drIPc/Jbc+IXd7QbZYTgsQpo2tztHdpYALgnnIeDL3uS7U8bFcrl+K1OxO5+jowJ60tqZgL2cPQRkcSGn3durtPAKG22+SkpQ6GgDamYaZwfwdh5d4fjPU3T+cqm203i1+Iktkf2UHKCMkNty7R3GQ+vTTQBccVPH9G12k2lq6+TtauZ0hF8reDGA8mMGjRw8TbW605TukqBSY4DiAdCNb8SCAdOh1+pRQgEIQgzOcLfpN+KjdAF1mdG0MDs4zFxBjs7MGgCzy62WxJIsDfRdPd9owlZZKh7r5nONy0m5JuWAtbe/QEgdAViAUntsSLg2JFxwNuY8EEHICYCmVJBjKYCCVJjSeAvoTp0GpP3IO32G3l12HWjv29OP8i8nuj+ifxZ6tR4c1dmH4vg+PwGJzWyEC7oZBlniNvdMINxx92w25eC8vBZYKh8bhJG9zHtN2vY4tc09WuGoKXCfDa5K7cS7zgdhVgUx1dnaTMwfFbbuvv8Y5bdDz7iloMGwCDO4tjJFjI/v1ExHIWFz8loAHgqnpd9eJspjA5sb5uDalw7wb/OjHdc7odB1BVfYniU9TIZqiV8sjuL3kk+odB0A0Cxq3qrF213yVdVmhoQaaE3Ge47dw+UNI/s3P85VhI65JJuSbknUkniSeZTdbSwPDW5vc9RpoOGmqi0XIF7eJ4DxNuS1qQQScEyoqWgQhCyEmkgIGhCFYPoAUHLIVErvYwTQu23Q4BTVtc6Cqj7SMU8rw3M5veBY0G7SDoHFcSrI3Ae+b/ok368S558ait28EiUgdFFybFj43svRsjwFzI7Gsy+cnO/0l3wX5938a8aW5dF9G97d62hd53RNPmrnBj2Al3m8nIEm5DHcr8Cbc2rYbR/itl/sfr0i+7HNsWUeO11JWDtKGpMLJmHUMLqaJvageqwdbWwB4tC5S1pS112uwuBU1RQYtPMzNJTwRvhdmcMjiJiTYGxvkbxv/AGr4N4Wx78NqMgOenkBfTy6EPZocpI0Lm3F+twea6Ddj72Y79Gi/VqFu3cTTXVuA0zcAgrwz8IdWOidJmdrHaXu5b5QLsaeH6VxKsquYTstSgAknECAALkkiewA6rY4Ns1BglM3FsUiMlSSBS0tu6ySxc10h1AeAL6+5tpd1rSUfBgux9Jh9L/COOMLnSNIpqG5bI820dJY3adRp8EcdSGrV4fgVJJgdbiBitNHVsZE7O89nG4w9wAmxFpXakX4dFzW0WPVNdO6pqn5nu0A4NY0cGRt+C0ftJuSSu2wP/FfEPpkX6aZLL9GxxndlFLg9LW0LCKkUzJpow5zjO1zQ57mtJPfBOgHEG3GyqBXLtLtHPh1NgFVBxbSPD2E2bJGW02aN3gbD1EA8lot5WzkEsTccw0fgs5BmjsAaeZxs4OA4AuNiOAdw0cFJVaPdZhEFXidPTVLM8Tu1LmXcL5YXubctIPEA8eS0GO07Y6meJgs1k0rGi97Na9wAueOgXXbkPfmm9U//AA8i5faj8sqvpE/tXJ9GrTQmG3vw0F+IHhpfjx4BEJCSFR9p0CxXQ9yiV3tYDlZO4D3zf9Em/XiVarvNzGMU1JiDpaqVsTHU8jA917Zi6MgaeDT9y55cajgWpFWaNiMA/l5v5ofvIOxGAfy8381/+ljyitntH+K2X+x+vSLld9Hv1V+uD/holuNo9oaJzMBbFOHikI7chrgWBskGpaRfURuNvBc7vRxKGpxWpqKd4kicYsrxezssMbHWv/OaR9SmPR0u7/GYMQpv4BxF2h1opzq6KUXyx3P15RzBLebV9WyGCz0VHtBS1DcsjKeIHo4ZajK5p5tI1B8VVtPLlPui0XFyACRbg4Akd4X0NxxVwUu8enqsHq4qx7W1ppzBmym9S2zuyIIHEF7rjkSToDpbNDPsptBDh+z0NXJCJnsqpRTtI0bO7tA15PIBufUa6+Nxpdj9uvPXzYdjT+0hrHd2Q2Hm8xsGZPiMuG25NIF9C4rS1+N0x2fgohIDO2tdI6OxzBmWXvcLW77fvXD3SY7G62u2bnw6pfSz6kaseBZssZvle37tRyII5LrsD/xXxD6ZF+mmRT7R0eJYY6jxOYR1VK0upKl4c7tG2/FPLQSToGnmRlOpBvrsIxumZs/W0bpAJ5KqN7I9buaDASQbWsOzd93ipaNhvT97cD+iP/Up1p92u2LaGV8FUO0oqgZKiMjMACMvaBvUA2IHEeIC+jeDjdNUUOEwwyh74KZzJmgG8bi2EAG443Y77lwl1ZwXRsxsccO2hpTGc9LM2d9NKDmDmmnkOQu5uAPHmLHqBVG1H5ZVfSJ/auVl7nt4kMAbRYi4CJhL6aZwv2Li1wcy/ENIc6x5ZiOB0rDHp2yVM8jDdr5pXNPVrnuINj4ELP1XwgoJQQktIEIQorM1IpkpBdmNGF3O53AqatxDsKuPtIxBI/KXOaMwLACcpB+EVw6sryf/AH0P0aX9aNZy4sB2n2Z/kWT887/qKUGLbKVDhFJQT0mbQTtlc4Rk/CcM508crlWkjhc+s/pUYo3PcGMBc5xDWtaCXOcTYAAakk8lm4q6Tb/Y5+G1AZnEsMre0gmHB7OhtpmFxe2liDzWgoqWSWRkMTS573NYxo4uc42aPvKszes3zfDsKw+axqY4i+QXBMbS0NDD4X06eiKw7o8Pjp2VGO1Q9FStc2EfHncLd3x7zWjxk8El9bG1263cUkGH56Mh1VRiPz3K5xLw9gc55YSQ21w8Wt3b8bKpHO0t6+Qvrbnx5f8Adyu93ebYvjxJ8tY4OjrXGOqv7m8hOR1jwa0uy25Ncei57b/Zx2HVstKb5Ac8JPwoX3yesixafFpWsbcfVSui3UYNQTRYhU11P27aWBsrW53t0Alc8DK4C5EY4rN//U7MfyLJ+ed++nuo978c+hf8qoVY3WO2qtWhdsviDxTNp56CV9mxy9oXMzHg12Z7mi501A9YXH4lspJR4mygqhmBmibmFwJIpHgBzTxFwSPAgjktJhWHTVMrKeBhfI9wa1oF9TzPQDiTwAuVam9apY7GsPhDg6SFtLHK8cS8zZg0/UQ77alVyG9jBqejxKWmpWdnE1sbmszOdYvja51i4k8bniuShic9wYxpc5xDWtaCXOcTYAAakk8laG9LAKqux6WnpYy95ZASeDWN7Jt3Pd8Fo/8AFzZbWujpdmI2hkfnOJTMJbO9hEMTeB7O/G3QHMeZaCAmxqqDYqhwuAVmPHPI8ehoGOOdx6yFpB/tyjmSTZVlVyMc97o2dmwucWszF2RpJLWZnausLC51NlmxbFZ6qV09TI6SR3unOOvqAGgA5AaBfGkQIcLG37D/AGhJJKppIQsjMmhC9DBhWV5P/vofo0v60arRdlun2lpsPr/OKouEfYyMu1pcczi0jQfJKzlxY3J3qwg+81Bz+A39xL/C/JGCaTDaKnkOgkbHqL9AA3+1Vq517pBTxi7bQyVeI1YzvdNUTva27ubnGw4aNaB00AHgre2q2iwnDo4sDlpHVbIGMc+0nZt7YgklwHFxzZugzjouB3Y49RUEs1ZUhzpmQkUrA27XSPBDi53wTazb9HuXMYw+odK6WqDxJN6Yl7S0vEl3B7QRq08iNFNbo707X7O/yGfzx/at1tjUU+O4W6upI3RzUBs+Nzg9/m5aC4l19RYZgTr6N45qm7k811e7TasYdWCSS5p5GmKoaBe7Dwdl5lpsfVmHNSz6Oq3J1YhpcXmLGyCOmZIY3i7HhjKh2Rw6G1ivj/wrwfyLQfm2/uLBgO0mHUjcZgidJ2VVC6Okuw3N2ygNd0t2g1PIfUq5U1segqPal9VhUtVgkFPT1cX5TCyFpkDNe/DawOgzC4N8rhxGtJ4DO+Svp5JHF73VUTnOcSXOcZWkkk6klZ9jNqJ8NqmVUOtu7IwmzZIye8w/dcHkQCtttVieGHFIq2gziEvinlYWZckgkzSNYOlhe3C5NjbhFd/vj2/lpJ5qChYIZHhjqipFhK/NGMrWEaizbDNxHK3E6LYvaenxKnGC4u7wpKonvxv4MYXH7gTxHdPJctvSx6CuxGWqpiTG5sbWlzS0nLG1p0OvEFckmhu9rtmajDqh1NUN1GrHj3MjL6PYenhyNwtIrJpNtKStw51BjJeZIml1JVMaXyAgWDJOp0AvezhxIIDlW91Qkk0lAIQhQZymGixNwLEWGtze9yNLaW5nmkVEruybj0SSQshhTUWjqndagTih7y7iSeA110AsB6rKJUmqdBZRKHOSSgSQks1QhCFkMpIQihCElKGEkIUEmWvrcDXgL6205jnZCihB9DnkgDTS/IX143PE8OfBQKZSsu9ZFkKbmjkb6DrzHDXpw+pQKlEiRpYHx14nw005JEpWQlCQ5ylm0tYcb35+r1LGlAhMIKypJJpLIEITd14eCBJJpJVCEIUAhCFBJr7Aiw1trzFuiEkIMwCRQUl6GDuiyYCZTSopPNze1vAX0+/VBKSzQKKZQFFMJFNRJQANjcJEoQsgTKSFAICElKp3QApslsHNsO9YXIBIsb90nh9SxqAUmNJIAFydABxJPIBRQgZCSzyuDmhznkvuG5SLgMa0BpDr/Va2lkIByQQgHVehg0iUiUlNqCgrK+YloZpYEkd1odd1r3cBmI0GhJtrbisRUA0jW4PDSxtY3Gp01Fr6aftSSFFTzC1ra3vfW/q42t9XIKBKCkpQJkfX4pICyJyx5TluD4ggjhfiFBCEUkJoUCQhZIm3v6ifuQY02nXhfwSQoGUJIQf/2Q=="
            };

            var metallica = new Product
            {
                Name = "Master Of Puppets",
                Category = metal,
                IsDeleted = false,
                Price = 80,
                ImgPath = "https://townsquare.media/site/838/files/2020/07/metallica-masterofpuppets.jpg"
            };

            var megadeth = new Product
            {
                Name = "Rust In Peace",
                Category = metal,
                IsDeleted = false,
                Price = 90,
                ImgPath = "https://img.discogs.com/SR9k38zxTjRtzo6h4aIGkNFJqtk=/fit-in/600x600/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-2538295-1549198316-2224.jpeg.jpg"
            };

            var omer = new Product
            {
                Name = "O.M.E.R",
                Category = mizrahit,
                IsDeleted = false,
                Price = 200,
                ImgPath = "https://www.israelhayom.co.il/sites/default/files/u55081/45444444444_0.jpg"
            };

            var michaelJackson = new Product
            {
                 Name = "Billie Jean",
                 Category = pop,
                 IsDeleted = false,
                 Price = 90,
                 ImgPath = "https://images-na.ssl-images-amazon.com/images/I/514no7ZRHBL.jpg"
            };

            var ram = new Product
            {
                Name = "Random Access Memories",
                Category = house,
                IsDeleted = false,
                Price = 128,
                ImgPath = "https://images-na.ssl-images-amazon.com/images/I/61Uxg-SWExL._SL1500_.jpg"
            };

            var eminemKamikaze = new Product
            {
                Name = "Kamikaze",
                Category = rap,
                IsDeleted = false,
                Price = 160,
                ImgPath = "https://upload.wikimedia.org/wikipedia/en/thumb/6/62/Eminem_-_Kamikaze.jpg/220px-Eminem_-_Kamikaze.jpg"
            };

            var eminemRelapse = new Product
            {
                Name = "Relapse",
                Category = rap,
                IsDeleted = false,
                Price = 90,
                ImgPath = "https://upload.wikimedia.org/wikipedia/en/4/42/Relapse_%28album%29.jpg"
            };

            Products.AddRange(
                rihanna,
                linkinPark,
                metallica,
                omer,
                megadeth,
                michaelJackson,
                ram,
                eminemKamikaze,
                eminemRelapse
            );

            var status = new OrderStatus
            {
                Name = "New"
            };

            var customer = new Costumer
            {
                Email = "cos1@gmail.com",
                FirstName = "cos",
                LastName = "tumer"
            };

            OrderStatuses.AddRange(status);

            SaveChanges();
        }

    }
}